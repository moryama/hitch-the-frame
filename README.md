# Hitch the Frame

This is a script I wrote for my own entertainment as a fan of Alfred Hitchcock. It lets me play a **guess-the-film game** with screenshots from his films.

This **command-line** program

- plays a nice intro to set the mood
- gathers a number of random images from an online archive of Hitchcock's films
- opens the images in the browser
- keeps playing the game until I have enough of it

## Tools

- Python 3.7
- [Requests](https://requests.readthedocs.io/en/master/index.html) to handle HTTP requests and scraping
- [Pygame](https://www.pygame.org/news) for a bit of music and sound effect

## Todos and beyond
- implement a minimal web interface

I am not planning to publish this game as I don't own any rights on the images it is based on, nor the server that hosts them.

## Usage

```
$ git clone https://gitlab.com/moryama/hitch-the-frame.git
$ cd hitch-the-frame
$ pip install requirements.txt
$ python3 play.py
```

**TIP:** If any Pygame warning message is spoiling the atmosphere for you, try running:
```
$ python3 play.py 2>/dev/null
```

## Screenshots

From the intro:

<img src="./img/intro.png" alt="Hitchcock profile" title="Hitchcock profile" /> 


## Credits
* for the massive work of image collection: [The Hitchcock Zone](https://the.hitchcock.zone/wiki/1000_Frames_of_Hitchcock) wiki
* for the intro march: [Raxlen Slice YT channel](https://www.youtube.com/channel/UChvAM2eixefAIHr7pX8McFQ)
* for the 'Good night' greeting: [Hitchcock Presents](https://www.youtube.com/channel/UCmF4lx6rO6yXAokeKYFtfEA), copyright by NBC Universal Television Distribution

## License
No license is available at the moment.
