import os
import os.path
import random
import webbrowser
import time

from helpers import capture_html


def prepare_data():    
    return make_url_list()

def make_url_list():
    '''Takes the src of 'The Hitchcock Zone' homepage.
    Returns a list of the urls of all films listed.''' 
    
    # The first time the game is run, build a file with the homepage src for future use
    if not os.path.isfile('home.txt'):
        capture_html('home.txt','https://the.hitchcock.zone/wiki/1000_Frames_of_Hitchcock')

    url_list = []
    with open('home.txt') as f:
        for line in f.readlines():        
            if 'wiki/1000_Frames_of_' in line:
                start = line.find('href=') + 6 
                finish = line.find('" title="') 
                url_list.append('https://the.hitchcock.zone' + line[start:finish]) 

    # We don't need the first url
    del url_list[0]
    return url_list


def make_random_url_list(url_list, user_num):
    '''Chooses random urls from the total url list, as many as the user requested''' 

    random_url_list = []
    count = 0
    for url in url_list: 
        if count != user_num:
            random_url_list.append(random.choice(url_list))
            count += 1
    return random_url_list


def make_random_frame_list(random_url_list):
    '''Searches the url of an image inside an html page.
    Returns a list of those urls.''' 

    random_frames = []
    for url in random_url_list:
        capture_html('frame_page.txt', (url + '_-_frame_' + str(random.choice(range(1, 999)))))

        with open('frame_page.txt') as f:
            for line in f.readlines():
                if 'img src="https://the.hitchcock.zone/1000' in line:
                    start = line.find('src=') + 5
                    finish = line.find('" alt="')
                    random_frames.append(line[start:finish])

        time.sleep(3)
    
    os.remove('frame_page.txt')
    return random_frames


def open_random_frames(url_list, user_num):
    '''Puts the previous actions in sequence 
    and finally opens the images url in the browser.''' 
    
    random_url_list = make_random_url_list(url_list, user_num)
    random_frames = make_random_frame_list(random_url_list)
    
    for frame_url in random_frames:
        webbrowser.open(frame_url)
        time.sleep(3)
