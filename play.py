from helpers import say

from tools import prepare_data
from tools import open_random_frames

from mood import play_intro


def play_game():

    say('''\nCheck your browser and make sure your navigation bar is hidden.''')
    while True:
        browser_checked = input("Are you done? (type 'yes') > ")
        if browser_checked == 'yes':
            break
    
    while True:
        try:
            user_num = int(input("How many films would you like to play with? (type a number 1-5) "))
            if user_num < 1 or user_num > 5:
                print('Please type a number 1-5.')
                continue
            say('I\'ll open some pictures in your browser. Try to guess the title of the film.')
            say('Then check your navigation bar for the right answer.')
            say('Ok, here we go...\n')
            break
        except ValueError:
            print('Please type a number.')
    
    urls = prepare_data()    
    open_random_frames(urls, user_num)

    while True:
        play_request = input("Would you like to play again? (yes or no) > ")
        if play_request == 'yes':
            say(('Ok, here we go...\n'))
            open_random_frames(urls, user_num)
        else:
            break    
    
    say("Ok, see you next time.")

play_intro()
play_game()