import pygame 
import time 
import sys
from helpers import say


def play_intro():
    '''Plays game intro with Hitchcock's profile and music.'''

    pygame.init()
    profile = open('props/profile.txt')

    title = " WELCOME TO 'HITCH THE FRAME' "
    print('\n' * 3)

    song = pygame.mixer.Sound('props/march.wav')
    clock = pygame.time.Clock()
    song.play()
    while True:
        for line in profile:
            sys.stdout.write(line)
            sys.stdout.flush()
            time.sleep(0.56)
        break

    profile.close()
    pygame.quit()

    pygame.init()
    good_evening = pygame.mixer.Sound('props/good_evening.wav')
    clock = pygame.time.Clock()
    good_evening.play()
    time.sleep(1)

    print('\n' * 2)
    say(('=' * 14) + title + ('=' * 15))
    print('\n')
    time.sleep(1)