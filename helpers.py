import requests
import time


def say(text):
    '''Prints with delay'''

    for char in text:
        print(char, end = '')
        time.sleep(0.025)
    print('')
    time.sleep(0.5)


def capture_html(filename, url):
    
    page_content = requests.get(url)
    page_file = open(filename, 'w')
    page_file.write(page_content.text)
    page_file.close()
    return True